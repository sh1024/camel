package com.example.demo;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.util.jndi.JndiContext;

public class CamelContentBasedRouter {
    public static void main(String[] args) throws Exception {

        JndiContext context = new JndiContext();
        context.bind("bye", new SayService());

        CamelContext camelContext = new DefaultCamelContext(context);

        camelContext.addRoutes(new RouteBuilder() {
            public void configure() {
                from("direct:hello")
                        .choice()
                        .when(body().contains("camel"))
                        .to("seda:small")
                        .otherwise()
                        .to("seda:large");
                from("seda:small").to("bean:bye");
                from("seda:large").to("bean:bye");

            }
        });

        camelContext.start();

        ProducerTemplate producer = camelContext.createProducerTemplate();
        producer.asyncRequestBody("direct:hello", "camel", String.class);
        producer.asyncRequestBody("direct:hello", "Something", String.class);

        Thread.sleep(11000L);
        camelContext.stop();
    }
}
