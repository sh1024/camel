package com.example.demo;

import org.apache.camel.Handler;

public class SayService {

    @Handler
    public void sayHello(String object) throws InterruptedException {
        Thread.sleep(5000L);
        System.out.println("Hello " + object);
    }
}
